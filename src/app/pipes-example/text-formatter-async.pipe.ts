import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Pipe({
  name: 'textFormatterAsync',
})
export class TextFormatterAsyncPipe implements PipeTransform {
  transform(value: string): Observable<string> {
    return of(`El valor ${value} se ha transformado usando un pipe`).pipe(delay(5000));
  }

}
