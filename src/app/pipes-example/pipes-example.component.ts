import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-example',
  templateUrl: './pipes-example.component.html',
})
export class PipesExampleComponent implements OnInit {

  nombre = 'Alvaro';
  fecha = new Date();
  dinero = 980;

  ngOnInit(): void {
  }

}
