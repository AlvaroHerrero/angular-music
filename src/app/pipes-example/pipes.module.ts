import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PipesExampleComponent } from './pipes-example.component';
import { TextFormatterAsyncPipe } from './text-formatter-async.pipe';
import { TextFormatterPipe } from './text-formatter.pipe';


@NgModule({
  declarations: [
    PipesExampleComponent,
    TextFormatterPipe,
    TextFormatterAsyncPipe,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: PipesExampleComponent},
    ]),
  ],
})
export class PipesModule {
}
