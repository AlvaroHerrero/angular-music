import { Pipe, PipeTransform } from '@angular/core';
import { MyService } from '../di-example/my.service';

@Pipe({
  name: 'textFormatter',
})
export class TextFormatterPipe implements PipeTransform {
  transform(value: string): string {
    return `El valor ${value} se ha transformado usando un pipe`;
  }

  constructor(myService: MyService) {
  }

}
