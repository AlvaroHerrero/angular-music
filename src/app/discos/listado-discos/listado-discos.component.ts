import { Component, OnInit } from '@angular/core';
import { Disco } from '../disco.model';

@Component({
  selector: 'app-listado-discos',
  templateUrl: './listado-discos.component.html',
  styleUrls: ['./listado-discos.component.css'],
})
export class ListadoDiscosComponent implements OnInit {

  discos: Disco[] = [
    {nombre: 'Thriller', artista: 'Michael Jackson'},
    {nombre: 'Do you belive', artista: 'Cher'},
    {nombre: 'Viva la vida', artista: 'Cannibal Corpse'},
  ]

  constructor() {
  }

  ngOnInit(): void {
  }

  showAlert(nombre: string) {
    console.log(nombre);
  }

}
