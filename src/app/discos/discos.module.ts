import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminGuard } from '../admin.guard';
import { FetchDiscosResolver } from '../fetch-discos.resolver';
import { DetalleComponent } from './detalle/detalle.component';
import { DiscoComponent } from './disco/disco.component';
import { DiscosComponent } from './discos/discos.component';
import { ListadoDiscosComponent } from './listado-discos/listado-discos.component';
import { ListadoFavoritosComponent } from './listado-favoritos/listado-favoritos.component';


@NgModule({
  declarations: [
    ListadoDiscosComponent,
    DiscoComponent,
    ListadoFavoritosComponent,
    DiscosComponent,
    DetalleComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: DiscosComponent,
        resolve: {discosDesdeResolver: FetchDiscosResolver},
        canActivate: [AdminGuard],
        data: {datosEstatico: 'Hola'},
      },
      {path: ':id', component: DetalleComponent, canActivate: [AdminGuard]},
    ]),
  ],
  providers: [],
  exports: [DiscosComponent],
})
export class DiscosModule {
}
