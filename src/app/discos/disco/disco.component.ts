import {
  AfterViewInit,
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-disco',
  templateUrl: './disco.component.html',
  styleUrls: ['./disco.component.css'],
})
export class DiscoComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  @Input() nombre!: string;

  get artista(): string {
    return this._artista;
  }

  private _artista!: string;
  @Input() set artista(value: string) {
    console.log(value);
    this._artista = value;
  };

  @Output() customEvent = new EventEmitter<string>();


  // @HostBinding('class.claseDesdeElHostBinding')
  // tieneClassRed = true;

  @HostBinding('class.claseDesdeElHostBinding')
  get tieneClassRed() {
    return this.nombre === 'Thriller';
  };

  // https://www.w3schools.com/tags/ref_eventattributes.asp
  @HostListener('click')
  callbackClick() {
    this.router.navigate(['discos', this.nombre])
  }


  // @HostBinding('style.width.px')
  // width = 20;


  constructor(private router: Router) {
    console.log('cons');
  }

  ngOnInit(): void {
    this.customEvent.emit(this.nombre);
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }


}
