import { Component, HostBinding, Input } from '@angular/core';
import { Cancion } from '../cancion.model';
import { Disco } from '../disco.model';

@Component({
  selector: 'app-listado-favoritos',
  templateUrl: './listado-favoritos.component.html',
  styleUrls: ['./listado-favoritos.component.css'],
})
export class ListadoFavoritosComponent {

  @Input() discos?: Disco[];
  @Input() canciones: Cancion[] = [];
  tieneQueMostrarDiscos = true;

  @HostBinding('style.border')
  get myBackgroundGetter() {
    return this.discos?.length === 0 ? '1px solid green' : 'initial';
  }

}
