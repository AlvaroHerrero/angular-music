import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DiscosService } from '../discos.service';

@Component({
  selector: 'app-discos',
  templateUrl: './discos.component.html',
  styleUrls: ['./discos.component.css'],
  providers: [DiscosService],
})
export class DiscosComponent {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    console.log(this.activatedRoute.snapshot.queryParams);
    console.log(this.activatedRoute.snapshot.data);
    console.log(this.activatedRoute.snapshot.fragment);
  }

  goToDirectivas() {
    this.router.navigate(['directivas'], {queryParams: {id: 23, hola: 'adios'}, fragment: 'footer'})
  }

}
