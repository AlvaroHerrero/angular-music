import { Injectable, OnDestroy } from '@angular/core';
import { interval, Subscription } from 'rxjs';

@Injectable()
export class DiscosService implements OnDestroy {

  sub = new Subscription()

  constructor() {
    this.sub.add(interval(1000).subscribe(console.log))
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
