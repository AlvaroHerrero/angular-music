import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ConfirmExitGuard } from '../confirm-exit.guard';
import { DiChildComponent } from './di-child/di-child.component';
import { DiExampleComponent } from './di-example.component';


@NgModule({
  declarations: [
    DiExampleComponent,
    DiChildComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: DiExampleComponent, canDeactivate: [ConfirmExitGuard]},
    ]),


  ],
})
export class DiExampleModule {
}
