import { InjectionToken } from '@angular/core';


export interface Config {
  debug: boolean,
  permisos: string[]
}

export const CONFIG = new InjectionToken<Config>('config.token');
