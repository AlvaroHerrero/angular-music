import { Component, Inject, LOCALE_ID } from '@angular/core';
import { CONFIG } from './config.injection-token';

@Component({
  selector: 'app-di-example',
  templateUrl: './di-example.component.html',
  styleUrls: ['./di-example.component.css'],
  providers: [{provide: CONFIG, useValue: {debug: true, permisos: []}}],
})
export class DiExampleComponent {


  constructor(@Inject(LOCALE_ID) locale: string,
  ) {
    // console.log(locale);
  }


}
