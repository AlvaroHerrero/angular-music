import { Component, Inject, LOCALE_ID } from '@angular/core';
import { MyService } from '../my.service';

@Component({
  selector: 'app-di-child',
  templateUrl: './di-child.component.html',
  styleUrls: ['./di-child.component.css'],
  providers: [{provide: LOCALE_ID, useValue: 'es-ES'}, MyService],
})
export class DiChildComponent {


  constructor(public myService: MyService,
              @Inject(LOCALE_ID) locale: string) {
  }

  cambiaValorDelServicio() {
    this.myService.valor += 1;
  }

}
