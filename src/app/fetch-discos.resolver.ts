import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Disco } from './discos/disco.model';

@Injectable({
  providedIn: 'root',
})
export class FetchDiscosResolver implements Resolve<Disco[]> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Disco[]> {
    return of([
      {nombre: 'Thriller', artista: 'Michael Jackson'},
      {nombre: 'Do you belive', artista: 'Cher'},
      {nombre: 'Viva la vida', artista: 'Cannibal Corpse'},
    ]);
  }

}
