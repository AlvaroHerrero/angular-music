import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-directives-example',
  templateUrl: './directives-example.component.html',
  styleUrls: ['./directives-example.component.css'],
})
export class DirectivesExampleComponent implements OnInit {


  constructor(
    private activatedRoute: ActivatedRoute) {
    console.log(this.activatedRoute.snapshot.queryParams);
    console.log(this.activatedRoute.snapshot.data);
    console.log(this.activatedRoute.snapshot.fragment);
  }

  ngOnInit(): void {
  }

  showAlert() {
    alert('alert desde una directiva usando un output')
  }

}
