import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ColorAfterClickDirective } from './color-after-click.directive';
import { DirectivesExampleComponent } from './directives-example.component';
import { DirectivesExampleDirective } from './directives-example.directive';

@NgModule({
  declarations: [
    DirectivesExampleComponent,
    DirectivesExampleDirective,
    ColorAfterClickDirective,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: DirectivesExampleComponent},
    ]),
  ],
})
export class DirectivesModule {
}
