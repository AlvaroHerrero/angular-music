import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appDirectivesExample]',
})
export class DirectivesExampleDirective {

  @Input() appDirectivesExample?: string;

  @Input()
  @HostBinding('style.color')
  myColor = 'blue';

  @HostListener('click')
  callbackClick() {
    if (this.appDirectivesExample) {
      navigator.clipboard.writeText(this.appDirectivesExample)
    }
  }

  constructor() {
  }

}
