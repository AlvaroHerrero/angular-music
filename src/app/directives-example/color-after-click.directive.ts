import {
  AfterViewInit,
  Directive,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Directive({
  selector: '[appColorAfterClick]',
})
export class ColorAfterClickDirective implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  @Input() appColorAfterClick?: string;
  @Input() color?: string;

  @Output()
  customEvent = new EventEmitter<string>();


  @HostBinding('style.border')
  get style() {
    return `1px solid ${this.color}`;
  }

  @HostListener('click', ['$event'])
  callbackClick(evt: PointerEvent) {
    this.customEvent.emit();
  }

  constructor() {
  }

  ngOnInit(): void {
    console.log('directiva en uso')
  }

  ngOnDestroy(): void {
  }

  ngAfterViewInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.color)
  }

}
