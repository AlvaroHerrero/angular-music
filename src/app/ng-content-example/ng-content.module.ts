import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgContentExampleComponent } from './ng-content-example.component';


@NgModule({
  declarations: [NgContentExampleComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: NgContentExampleComponent},
    ]),
  ],
})
export class NgContentModule {
}
