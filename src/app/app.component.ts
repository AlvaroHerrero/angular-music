import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Disco } from './discos/disco.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [],
})
export class AppComponent {

  discos: Disco[] = [
    {nombre: 'WAR', artista: 'U2'},
    {nombre: 'Back in Black', artista: 'ACDC'},
  ]

  constructor(router: Router) {
    router.events
      .pipe(
        filter(evt => evt instanceof NavigationEnd),
      )
      .subscribe(evt => {
      })
  }

}
