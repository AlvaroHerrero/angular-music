import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css'],
})
export class FormsComponent implements OnInit {

  myForm: FormGroup = this.formBuilder.group(
    // Configuracion de controls
    {
      nombre: ['', {
        validators: [Validators.required],
        asyncValidators: [myValidatorAsync],
      }],
      apellidos: ['', [Validators.required, Validators.maxLength(12), myValidatorConArgumentos(23)]],
      aceptoCondiciones: ['', [Validators.requiredTrue]],
      mensajes: this.formBuilder.array([
        new FormControl('mensaje por defecto'),
      ]),
    },
    // Opciones del form
    {
      validators: [],
      asyncValidators: [],
      updateOn: 'blur',  // por defecto
    })

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) {
  }

  get mensajes() {
    return this.myForm.get('mensajes') as FormArray;
  }

  addMessage() {
    this.mensajes.push(new FormControl('mensaje por defecto'));
  }

  ngOnInit(): void {
    this.myForm?.statusChanges.subscribe(v => console.log(v));

    this.myForm.valueChanges.subscribe(v => {
      this.myForm.patchValue({nombre: 'Manolo'}, {emitEvent: false});
    });

    this.myForm.get('nombre')?.setValue('Alvaro');
    this.myForm.get('nombre')?.disable();

  }

  submit() {

    const body = this.myForm.getRawValue();

    this.httpClient.post('myurl', body)

    if (this.myForm.status === 'VALID') {
      alert('form submit')
    }
  }

}

function myFormValidator(formGroup: FormGroup): null|ValidationErrors {
  return formGroup.get('nombre')?.value === 'A' && formGroup.get('apellido')?.value === 'B' ? null : {formValidator: 'Mensaje de error'};
}

function myValidator(formControl: FormControl): null|ValidationErrors {
  return formControl.value === 'Pedro' ? null : {'myValidator': 'El nombre tiene que ser Pedro'};
}

function myValidatorConArgumentos(num: number): ValidatorFn {
  return (formControl: AbstractControl): null|ValidationErrors => {
    return formControl.value === 'Pedro' ? null : {'myValidator': 'El nombre tiene que ser Pedro'};
  }
}

function myValidatorAsync(formControl: FormControl): Observable<null|ValidationErrors> {
  return of(formControl.value === 'Pedro' ? null : {myValidatorAsync: 'El nombre tiene que ser Pedro'}).pipe(delay(5000));
}
