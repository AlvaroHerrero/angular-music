import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpComponent } from './http.component';


@NgModule({
  declarations: [
    HttpComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: HttpComponent},
    ]),
  ],
})
export class HttpModule {
}
