import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-http',
  templateUrl: './http.component.html',
  styleUrls: ['./http.component.css'],
})
export class HttpComponent implements OnInit {

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit(): void {

    this.httpClient.get('todos/', {
      params: {
        name: 'ss',
      },
    }).subscribe(value => {
      console.log(value);
    });

    this.httpClient.post('posts/',
      {bodyField1: 'Hola', bodyField2: 'Adios'},
      {
        params: {
          name: 'ss',
        },
        headers: {
          myHeader: 'MyHeaderValue',
        },
      })
      .subscribe(value => {
        console.log(value);
      })
  }

}
