import { registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import localeEs from '@angular/common/locales/es';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Route, RouterModule } from '@angular/router';
import { AdminGuard } from './admin.guard';
import { AppComponent } from './app.component';
import { MyInterceptorInterceptor } from './my-interceptor.interceptor';

registerLocaleData(localeEs, 'es-ES');


const routes: Route[] = [
  {
    path: 'discos',
    loadChildren: () => import('./discos/discos.module').then(m => m.DiscosModule),
    data: {title: 'Discos'},
  },
  {
    path: 'directivas',
    loadChildren: () => import('./directives-example/directives.module').then(m => m.DirectivesModule),
    canLoad: [AdminGuard],
  },
  {path: 'di', loadChildren: () => import('./di-example/di-example.module').then(m => m.DiExampleModule)},
  {
    path: 'ng-content',
    loadChildren: () => import('./ng-content-example/ng-content.module').then(m => m.NgContentModule),
  },
  {path: 'pipes', loadChildren: () => import('./pipes-example/pipes.module').then(m => m.PipesModule)},
  {path: 'http', loadChildren: () => import('./http/http.module').then(m => m.HttpModule)},
  {path: 'forms', loadChildren: () => import('./forms/my-forms.module').then(m => m.MyFormsModule)},
  {path: 'rxjs', loadChildren: () => import('./rxjs/rxjs.module').then(m => m.RxjsModule)},
  {path: '', redirectTo: 'discos', pathMatch: 'full'},
  {path: '**', redirectTo: 'discos'},
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: MyInterceptorInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent],
})
export class AppModule {
}
