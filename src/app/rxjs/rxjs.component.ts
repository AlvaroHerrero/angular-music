import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, combineLatest, fromEvent, interval, Subject, Subscription } from 'rxjs';
import { concatMap, finalize, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.css'],
})
export class RxjsComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('buton1') button1?: ElementRef;
  @ViewChild('buton2') button2?: ElementRef;

  private subscription = new Subscription();
  private behavioursubject = new BehaviorSubject<string>('Hola');
  private subject = new Subject<void>();

  constructor() {
  }

  ngOnInit(): void {

    // interval(400)
    //   .pipe(
    //     map((value, index) => index === 0 ? 'Hola' : value),
    //     filter((value, index) => value !== 5),
    //     // first((value) => value === 10),
    //     take(5),
    //     finalize(() => console.log('completed')),
    //   )
    //   .subscribe({
    //     next: (e) => console.log(e),
    //     error: (e) => console.log(e),
    //     complete: () => console.log('completed'),
    //   })
  }

  ngAfterViewInit(): void {


    this.subject.subscribe(console.log)
    this.behavioursubject.next('Adios');
    this.behavioursubject.subscribe(console.log)
    this.behavioursubject.next('Adios 2');


    const button2$ = fromEvent(this.button2?.nativeElement, 'click');

    this.subscription.add(fromEvent(this.button1?.nativeElement, 'click')
      .pipe(
        // map(() => 'boton 1'),
        // switchMap, exhaustMap, mergeMap
        concatMap((texto) => interval(300).pipe(
          takeUntil(button2$),
        )),
        finalize(() => alert(1)),
      )
      .subscribe((v) => {
        console.log(v)
      }))

    combineLatest([button2$, this.behavioursubject]).subscribe(console.log);
    // forkJoin, zip
  }

  ngOnDestroy(): void {
    // this.subscription.unsubscribe();
    this.subject.next();
  }

}
