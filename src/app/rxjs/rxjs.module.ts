import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RxjsComponent } from './rxjs.component';


@NgModule({
  declarations: [
    RxjsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: RxjsComponent},
    ]),
  ],
})
export class RxjsModule {
}
